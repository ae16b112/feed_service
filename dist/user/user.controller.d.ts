import { User as UserModel } from '@prisma/client';
import { UserService } from './user.service';
import { RegisterDto } from './dto/register.dto';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    signupUser({ username, password, email }: RegisterDto): Promise<UserModel>;
    getProfile(username: string): Promise<UserModel>;
}
