import { Post as PostModel } from '@prisma/client';
import { PostService } from './post.service';
import { CreatePostDto } from './dto/create_post.dto';
import { UpdatePostDto } from './dto/update_post.dto';
import { SearchPostDto } from './dto/search_post.dto';
export declare class PostController {
    private readonly postService;
    constructor(postService: PostService);
    getPosts(): Promise<PostModel[]>;
    getPostById(id: string): Promise<PostModel>;
    getPostByTitle(title: string): Promise<PostModel>;
    searchPosts(query: SearchPostDto): Promise<void>;
    createPost({ title, description, image, tags }: CreatePostDto, req: any): Promise<PostModel>;
    updatePost(id: string, req: any, { title, description, image, tags }: UpdatePostDto): Promise<PostModel>;
    deletePost(id: string): Promise<PostModel>;
}
