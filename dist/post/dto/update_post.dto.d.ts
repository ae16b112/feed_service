export declare class UpdatePostDto {
    title: string;
    description: string;
    image: string;
    tags: string;
}
