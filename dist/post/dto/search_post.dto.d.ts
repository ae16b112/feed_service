export declare class SearchPostDto {
    title: string;
    username?: string;
    tags?: string;
    startDate?: Date;
    endDate?: Date;
}
