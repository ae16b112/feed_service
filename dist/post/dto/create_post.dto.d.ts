export declare class CreatePostDto {
    title: string;
    description: string;
    image: string;
    tags: string[];
}
