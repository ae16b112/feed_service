"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentsService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const comment_event_emitter_1 = require("./comment.event-emitter");
let CommentsService = class CommentsService {
    constructor(prisma, commentEventEmitter) {
        this.prisma = prisma;
        this.commentEventEmitter = commentEventEmitter;
    }
    async createComment(content, username, postId) {
        const comment = await this.prisma.comment.create({
            data: {
                text: content,
                author: {
                    connect: {
                        username,
                    },
                },
                post: {
                    connect: {
                        id: parseInt(postId),
                    },
                },
            },
            include: {
                author: true,
            },
        });
        this.commentEventEmitter.emitCommentCreatedEvent(comment);
        return comment;
    }
    async deleteComment(where) {
        const comment = await this.prisma.comment.delete({
            where,
        });
        this.commentEventEmitter.emitCommentDeletedEvent(comment);
        return comment;
    }
    async getCommentsForPost(postId) {
        const comments = await this.prisma.comment.findMany({
            where: {
                postId: parseInt(postId),
            },
            include: {
                author: true,
                post: true
            },
        });
        return comments;
    }
};
CommentsService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService, comment_event_emitter_1.CommentEventEmitter])
], CommentsService);
exports.CommentsService = CommentsService;
//# sourceMappingURL=comments.service.js.map