"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentEventEmitter = void 0;
const common_1 = require("@nestjs/common");
const callback_api_1 = require("amqplib/callback_api");
let CommentEventEmitter = class CommentEventEmitter {
    constructor() {
        this.exchange = 'comment_exchange';
        (0, callback_api_1.connect)('amqp://localhost', (err, connection) => {
            if (err) {
                console.error(err);
                return;
            }
            connection.createChannel((err, channel) => {
                if (err) {
                    console.error(err);
                    return;
                }
                channel.assertExchange(this.exchange, 'fanout', { durable: false });
                this.emitCommentCreatedEvent = (comment) => {
                    const message = JSON.stringify({
                        type: 'comment_created',
                        payload: comment,
                    });
                    channel.publish(this.exchange, '', Buffer.from(message));
                };
                this.emitCommentDeletedEvent = (comment) => {
                    const message = JSON.stringify({
                        type: 'comment_deleted',
                        payload: comment,
                    });
                    channel.publish(this.exchange, '', Buffer.from(message));
                };
            });
        });
    }
    emitCommentCreatedEvent(comment) { }
    emitCommentDeletedEvent(comment) { }
};
CommentEventEmitter = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], CommentEventEmitter);
exports.CommentEventEmitter = CommentEventEmitter;
//# sourceMappingURL=comment.event-emitter.js.map