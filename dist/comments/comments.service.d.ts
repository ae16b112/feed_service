import { Comment, Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { CommentEventEmitter } from './comment.event-emitter';
export declare class CommentsService {
    private prisma;
    private commentEventEmitter;
    constructor(prisma: PrismaService, commentEventEmitter: CommentEventEmitter);
    createComment(content: string, username: string, postId: string): Promise<Comment>;
    deleteComment(where: Prisma.CommentWhereUniqueInput): Promise<Comment>;
    getCommentsForPost(postId: string): Promise<Comment[]>;
}
