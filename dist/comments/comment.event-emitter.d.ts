import { Comment } from '@prisma/client';
export declare class CommentEventEmitter {
    private readonly exchange;
    constructor();
    emitCommentCreatedEvent(comment: Comment): void;
    emitCommentDeletedEvent(comment: Comment): void;
}
