"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentEventHandler = void 0;
const common_1 = require("@nestjs/common");
const callback_api_1 = require("amqplib/callback_api");
const prisma_service_1 = require("../prisma/prisma.service");
let CommentEventHandler = class CommentEventHandler {
    constructor(prisma) {
        this.prisma = prisma;
        this.exchange = 'comment_exchange';
        (0, callback_api_1.connect)('amqp://localhost', (err, connection) => {
            if (err) {
                console.error(err);
                return;
            }
            connection.createChannel((err, channel) => {
                if (err) {
                    console.error(err);
                    return;
                }
                channel.assertExchange(this.exchange, 'fanout', { durable: false });
                channel.assertQueue('', { exclusive: true }, (err, q) => {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    channel.bindQueue(q.queue, this.exchange, '');
                    channel.consume(q.queue, async (msg) => {
                        const { type, payload } = JSON.parse(msg.content.toString());
                        switch (type) {
                            case 'comment_created':
                                await this.handleCommentCreated(payload);
                                break;
                            case 'comment_deleted':
                                await this.handleCommentDeleted(payload);
                                break;
                            default:
                                console.warn(`Unknown event type: ${type}`);
                        }
                    }, { noAck: true });
                });
            });
        });
    }
    async handleCommentCreated(comment) {
        const post = await this.prisma.post.findUnique({
            where: {
                id: comment.postId
            }
        });
        if (post) {
            await this.prisma.post.update({
                where: {
                    id: comment.postId,
                },
                data: {
                    commentsCount: post.commentsCount + 1,
                },
            });
        }
    }
    async handleCommentDeleted(comment) {
        const post = await this.prisma.post.findUnique({
            where: {
                id: comment.postId
            }
        });
        if (post) {
            await this.prisma.post.update({
                where: {
                    id: comment.postId,
                },
                data: {
                    commentsCount: {
                        decrement: 1,
                    },
                },
            });
        }
    }
};
CommentEventHandler = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], CommentEventHandler);
exports.CommentEventHandler = CommentEventHandler;
//# sourceMappingURL=comment.event-handler.js.map