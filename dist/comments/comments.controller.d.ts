import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { Comment as CommentModel } from '@prisma/client';
export declare class CommentsController {
    private readonly commentsService;
    constructor(commentsService: CommentsService);
    createComment(postId: string, { content }: CreateCommentDto, req: any): Promise<CommentModel>;
    getCommentsForPost(postId: string): Promise<CommentModel[]>;
    deleteComment(id: number): Promise<CommentModel>;
}
