import { Comment as CommentModel } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
export declare class CommentEventHandler {
    private prisma;
    private readonly exchange;
    constructor(prisma: PrismaService);
    handleCommentCreated(comment: CommentModel): Promise<void>;
    handleCommentDeleted(comment: CommentModel): Promise<void>;
}
