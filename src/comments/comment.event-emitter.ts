import { Injectable } from '@nestjs/common';
import { connect } from 'amqplib/callback_api';
import { Comment } from '@prisma/client';

@Injectable()
export class CommentEventEmitter {
  private readonly exchange = 'comment_exchange';

  constructor() {
    connect('amqp://localhost', (err, connection) => {
      if (err) {
        console.error(err);
        return;
      }

      connection.createChannel((err, channel) => {
        if (err) {
          console.error(err);
          return;
        }

        channel.assertExchange(this.exchange, 'fanout', { durable: false });

        this.emitCommentCreatedEvent = (comment: Comment) => {
          const message = JSON.stringify({
            type: 'comment_created',
            payload: comment,
          });

          channel.publish(this.exchange, '', Buffer.from(message));
        };

        this.emitCommentDeletedEvent = (comment: Comment) => {
          const message = JSON.stringify({
            type: 'comment_deleted',
            payload: comment,
          });

          channel.publish(this.exchange, '', Buffer.from(message));
        };
      });
    });
  }

  emitCommentCreatedEvent(comment: Comment) {}
  emitCommentDeletedEvent(comment: Comment) {}
}