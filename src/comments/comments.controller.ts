import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { User as UserModel, Post as PostModel, Comment as CommentModel } from '@prisma/client';

@Controller('posts/:postId/comments')
@ApiTags('comments')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Post('create')
  async createComment(@Param('postId') postId: string, @Body() {content}:CreateCommentDto, @Req() req):Promise<CommentModel> {
    const { username } = req.user;

    return this.commentsService.createComment(content, username, postId);
  }

  @Get()
  async getCommentsForPost(@Param('postId') postId: string) {
    return this.commentsService.getCommentsForPost(postId);
  }

  @Delete(':id')
  async deleteComment(@Param('id') id: number) {

    return this.commentsService.deleteComment({ id: Number(id) });
  }
}
