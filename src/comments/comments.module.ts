import { Module } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CommentsController } from './comments.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { CommentEventHandler } from './comment.event-handler';
import { CommentEventEmitter } from './comment.event-emitter';

@Module({
  imports: [PrismaModule],
  controllers: [CommentsController],
  providers: [CommentsService, CommentEventHandler, CommentEventEmitter]
})
export class CommentsModule {}
