import { Injectable } from '@nestjs/common';
import { connect } from 'amqplib/callback_api';
import { User as UserModel, Post as PostModel, Comment as CommentModel } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class CommentEventHandler {
  private readonly exchange = 'comment_exchange';

  constructor(private prisma: PrismaService) {
    connect('amqp://localhost', (err, connection) => {
      if (err) {
        console.error(err);
        return;
      }

      connection.createChannel((err, channel) => {
        if (err) {
          console.error(err);
          return;
        }

        channel.assertExchange(this.exchange, 'fanout', { durable: false });

        channel.assertQueue('', { exclusive: true }, (err, q) => {
          if (err) {
            console.error(err);
            return;
          }

          channel.bindQueue(q.queue, this.exchange, '');

          channel.consume(q.queue, async (msg) => {
            const { type, payload } = JSON.parse(msg.content.toString());

            switch (type) {
              case 'comment_created':
                await this.handleCommentCreated(payload);
                break;
              case 'comment_deleted':
                await this.handleCommentDeleted(payload);
                break;
              default:
                console.warn(`Unknown event type: ${type}`);
            }
          }, { noAck: true });
        });
      });
    });
  }

  async handleCommentCreated(comment: CommentModel) {
    const post= await this.prisma.post.findUnique({
        where:{
            id:comment.postId
        }
    })
    if(post){
        await this.prisma.post.update({
            where: {
              id: comment.postId,
            },
            data: {
              commentsCount: post.commentsCount+1,
            },
        });
    }

  }

  async handleCommentDeleted(comment: CommentModel) {
    const post= await this.prisma.post.findUnique({
        where:{
            id:comment.postId
        }
    })
    if(post){
        await this.prisma.post.update({
            where: {
              id: comment.postId,
            },
            data: {
              commentsCount: {
                decrement: 1,
              },
            },
        });
    }
  }
}