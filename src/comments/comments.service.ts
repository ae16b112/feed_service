import { Injectable } from '@nestjs/common';
import { PrismaClient, Comment, Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { CommentEventEmitter } from './comment.event-emitter';

@Injectable()
export class CommentsService {
  constructor(private prisma: PrismaService, private commentEventEmitter: CommentEventEmitter) {}

  async createComment(content: string, username: string, postId: string): Promise<Comment> {

    const comment = await this.prisma.comment.create({
      data: {
        text: content,
        author: {
          connect: {
            username,
          },
        },
        post: {
          connect: {
            id: parseInt(postId),
          },
        },
      },
      include: {
        author: true,
      },
    });
    this.commentEventEmitter.emitCommentCreatedEvent(comment);
    return comment;
  }

  async deleteComment(where: Prisma.CommentWhereUniqueInput): Promise<Comment> {
    const comment = await this.prisma.comment.delete({
      where,
    });
    this.commentEventEmitter.emitCommentDeletedEvent(comment);
    return comment;
  }

  async getCommentsForPost(postId: string): Promise<Comment[]> {
    const comments = await this.prisma.comment.findMany({
      where: {
        postId:parseInt(postId),
      },
      include: {
        author: true,
        post: true
      },
    });

    return comments;
  }
}