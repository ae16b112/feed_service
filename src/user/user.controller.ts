
import {
    Controller,
    Get,
    Param,
    Post,
    Body,
    Put,
    Delete,
    UseGuards,
  } from '@nestjs/common';
import { User as UserModel, Post as PostModel } from '@prisma/client';
import { UserService } from './user.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ApiCreatedResponse, ApiOkResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { RegisterDto } from './dto/register.dto';
import { UserEntity } from './entity/user.entity';


  @Controller('user')
  @ApiTags('user')
  export class UserController {
    constructor(
      private readonly userService: UserService,
    ) {}
  
    @Post('register')
    async signupUser(
      @Body() { username, password, email }: RegisterDto,
    ): Promise<UserModel> {
      return this.userService.createUser({
          username: username,
          password: password,
          email: email
      });
    }

    @Get(':username')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiOkResponse({type: UserEntity})
    async getProfile(
        @Param('username') username: string,
    ): Promise<UserModel> {
      return this.userService.findOne({username:username});
    }

  }