//src/auth/auth.service.ts
import {
    Injectable,
    NotFoundException,
    UnauthorizedException,
  } from '@nestjs/common';
  import { PrismaService } from './../prisma/prisma.service';
  import { JwtService } from '@nestjs/jwt';
  import { AuthEntity } from './entity/auth.entity';
  
  @Injectable()
  export class AuthService {
    constructor(private prisma: PrismaService, private jwtService: JwtService) {}
  
    async login(username: string, password: string): Promise<AuthEntity> {
      const user = await this.prisma.user.findUnique({ where: { username: username } });
  
      if (!user) {
        throw new NotFoundException(`No user found for email: ${username}`);
      }
      const isPasswordValid = user.password === password;
  
      // If password does not match, throw an error
      if (!isPasswordValid) {
        throw new UnauthorizedException('Invalid password');
      }
  
      // Step 3: Generate a JWT containing the user's ID and return it
      return {
        accessToken: this.jwtService.sign({ username: user.username }),
      };
    }
  }