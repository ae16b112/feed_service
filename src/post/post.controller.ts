
import {
    Controller,
    Get,
    Param,
    Post,
    Body,
    Put,
    Delete,
    UseGuards,
    Query,
    Req
  } from '@nestjs/common';
import { User as UserModel, Post as PostModel } from '@prisma/client';
import { PostService } from './post.service';
import { ApiCreatedResponse, ApiOkResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreatePostDto } from './dto/create_post.dto';
import { UpdatePostDto } from './dto/update_post.dto';
import { SearchPostDto } from './dto/search_post.dto';

  @Controller("posts")
  @ApiTags('posts')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  export class PostController {
    constructor(
      private readonly postService: PostService,
    ) {}
  
    @Get()
    async getPosts() {
      return this.postService.posts({
        orderBy: {
          createdAt: 'desc',
        },
      });
    }
  
    @Get(':id')
    async getPostById(@Param('id') id: string): Promise<PostModel> {
      return this.postService.post(id=id);
    }

    @Get(':title')
    async getPostByTitle(@Param('title') title: string) {
        return this.postService.post(title=title);
    }
  
    @Get('search')
    async searchPosts(
      @Query() { title, tag, username, startDate, endDate}:SearchPostDto
    ) {
      const where = {
        title: title ? { contains: title, mode: 'insensitive' } : undefined,
        tags: tag ? { contains: tag, mode: 'insensitive' } : undefined,
        author: username ? { username } : undefined,
        createdAt: {
          gte: startDate ? new Date(startDate) : undefined,
          lte: endDate ? new Date(endDate) : undefined,
        },
      };
  
      return this.postService.posts({
        where,
        orderBy: {
          createdAt: 'desc',
        },
      });
    }
  
    @Post('create')
    async createPost(
        @Body(){ title, description, image, tags}:CreatePostDto,
        @Req() req,
    ): Promise<PostModel> {
      const { username } = req.user;
      return this.postService.createPost({
        title,
        description,
        image,
        tags:tags?.join(','),
        author: {
          connect: { username },
        },
      });
    }
    
    @Put('update/:id')
    async updatePost(
        @Param('id') id: string, 
        @Req() req, 
        @Body() { title, description, image, tags }:UpdatePostDto
    ) { 
        
      const post = await this.postService.updatePost({
        where: {
          id:parseInt(id, 10),
        },
        data: {
          title,
          description,
          image,
          tags,
        },
      });
  
      return post;
    }

    @Delete('delete/:id')
    async deletePost(@Param('id') id: string): Promise<PostModel> {
      return this.postService.deletePost({ id: Number(id) });
    }
  }