//src/auth/dto/login.dto.ts
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsUrl, IsString, IsArray, MinLength, MaxLength } from 'class-validator';

export class CreatePostDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  @MinLength(3)
  @MaxLength(20)
  title: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  @MinLength(10)
  @MaxLength(3000)
  description: string;

  @IsUrl()
  @IsNotEmpty()
  @ApiProperty()
  image: string;

  @IsArray()
  @IsString({ each: true })
  @ApiProperty({ type: [String] })
  tags: string[];
}