//src/auth/dto/login.dto.ts
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsNotEmpty, IsEmpty, IsString, MinLength, IsArray, IsDate } from 'class-validator';

export class SearchPostDto {
  @IsString()
  @IsEmpty()
  @ApiProperty()
  // @IsOptional()
  title: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  username?: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  tag?: string;

  @IsDate()
  @ApiProperty()
  @IsOptional()
  startDate?: Date;

  @IsDate()
  @IsOptional()
  @ApiProperty()
  endDate?: Date;
}
