//src/auth/dto/login.dto.ts
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength,MaxLength, IsUrl } from 'class-validator';

export class UpdatePostDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  @MinLength(3)
  @MaxLength(20)
  title: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  @MinLength(10)
  @MaxLength(3000)
  description: string;

  @IsUrl()
  @IsNotEmpty()
  @ApiProperty()
  image: string;

  @IsString()
  @ApiProperty()
  tags: string;
}